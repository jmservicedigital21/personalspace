import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './component/homepage/homepage.component';
import { LoginComponent } from './component/login/login.component';
import { ContactComponent } from './component/contact/contact.component';
import { LocationComponent } from './component/location/location.component';
import { AlbumPhotoComponent } from './component/album-photo/album-photo.component';

const routes: Routes = [
  {path: '', component: HomepageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'contact', component: ContactComponent},
  { path: 'location', component: LocationComponent },
  {path: 'album', component: AlbumPhotoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],




exports: [RouterModule]
})
export class AppRoutingModule { }
