import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { User } from 'firebase';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/Operators';
import { DbService } from './../../services/db.service';

@Component({
  selector: 'app-album-photo',
  templateUrl: './album-photo.component.html',
  styleUrls: ['./album-photo.component.scss']
})
export class AlbumPhotoComponent implements OnInit , OnDestroy{
  photo = { title: '', file: '' }
  user: User;
  userSub: Subscription;
  photoServerUrl;
  uploadedImgURL;

  constructor(private afAuth: AngularFireAuth, private afStorage: AngularFireStorage, private db: DbService) { }

  ngOnInit(): void {
    this.db.readPersonalSpaces().subscribe(data => {
      console.log('data', data);
    }, err => {
        console.error('err', err);
    });
    this.userSub = this.afAuth.authState.subscribe(user => {
      this.user = user;
      if (this.user) {
        this.db.readPersonalSpaceByUID(user.uid).subscribe(data => {
          console.log('ngOnInit() readPersonalSpaceByUID() /data ', data);
          if (!data || data.length === 0) {
            this.db.createPersonalSpace(this.user);
          }
        }, err => {
          console.error(err);
        });
      }
    });
  }
  onFileChange(e) {
    console.log(e.target.files[0]);
    this.photo.file = e.target.files[0];
   }
  postPhoto() {

    const uid = this.user.uid;
    const photoPathOnServer = `personal-space/${uid}/${this.photo.title}`;
    const photoRef = this.afStorage.ref(photoPathOnServer);
    const currentUpload = this.afStorage.upload(photoPathOnServer, this.photo.file);
    currentUpload.catch(err => console.error(err));
    currentUpload.snapshotChanges().pipe(
      finalize(() => {
        this.photoServerUrl = photoRef.getDownloadURL();
        console.log('photoServerUrl', this.photoServerUrl);

        this.photoServerUrl.subscribe(data => {
          console.log('data', data);
          this.uploadedImgURL = data;
          this.db.updatePersonalSpacePhotoURLs(this.user, this.uploadedImgURL);
        });
      })
    ).subscribe();
    this.photo = { file: '', title: '' };
   }

  ngOnDestroy() {
    this.userSub.unsubscribe();
  }
}
