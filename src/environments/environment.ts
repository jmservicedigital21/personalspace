// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyAXq33VZurpAA6qWw-PeJTPTIo-4EbHBaM",
  authDomain: "personalspace-aa539.firebaseapp.com",
  databaseURL: "https://personalspace-aa539.firebaseio.com",
  projectId: "personalspace-aa539",
  storageBucket: "personalspace-aa539.appspot.com",
  messagingSenderId: "796925560121",
  appId: "1:796925560121:web:6bf96b9b24f488c91514e8",
  measurementId: "G-4GZBJNN24Z"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
